﻿using System;


namespace CollectionApp
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                var secondExample = new CollectionManager<char>();
                var signs = new char[] { 'g', 't', 'q', 't' };
                secondExample.Generate(signs);
                secondExample.Sort(med => med > 'k');
                secondExample.Show();
                secondExample.BinarySerialize();
                secondExample.BinaryDeserialize();
                secondExample.XmlSerialize();
                secondExample.XmlDeserialize();
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                Console.ReadKey();
            }
        }
    }
}
