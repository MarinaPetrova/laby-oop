﻿using MyCollectionBl;
using System;
using System.Collections.Generic;


namespace CollectionApp
{
    class CollectionManager<TValue> where TValue : struct
    {
        private MyCollection<TValue> _collection;

        public CollectionManager()
        {
            _collection = new MyCollection<TValue>();
        }
        public void Generate(TValue[] values)
        {
            for (int i = 0; i < values.Length; ++i)
                _collection.AddValue(values[i]);
        }
        public void Sort(Func<TValue,bool> validateCount)
        {
            Comparer<TValue> defSort = Comparer<TValue>.Default;
            _collection.SortCollection(defSort,validateCount);
        }
        public void Show()
        {
            string result=_collection.ToString();
            Console.WriteLine(result);
        }
        public void BinarySerialize()
        {
            var output = _collection.BinarySerialize();
            Console.WriteLine(output);
        }
        public void BinaryDeserialize()
        {
            var output = _collection.BinaryDeserialize();
            Console.WriteLine(output);
        }
        public void XmlSerialize()
        {
            var output = _collection.XmlSerialize();
            Console.WriteLine(output);
        }
        public void XmlDeserialize()
        {
            var output = _collection.XmlDeserialize();
            Console.WriteLine(output);
        }
    }
}
