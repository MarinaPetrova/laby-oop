﻿using Lab_2.Manager;
using System;

namespace Lab_2
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                Create();
            }
            catch (FormatException)
            {
                Console.WriteLine("\aError!");
            }
            finally
            {
                Console.ReadKey();
            }
            
        }
        static void Create()
        {
            var example = new MainManager();
            example.Output();
        }
    }
}
