﻿using System;

namespace FigureBALL
{
    public class Figure 
    {
        float _fNumber;
        float _sNumber;
        float _lNumber;
        public float Avalue{ get { return _fNumber; } }
        public float Svalue{ get { return _sNumber; } }
        public float Lvalue { get { return _lNumber; } }
         public Figure (float _fNumber, float _sNumber, float _lNumber)
        {
            this._fNumber = _fNumber;
            this._sNumber = _sNumber;
            this._lNumber = _lNumber;
        }
         public float Square (float fValue, float sValue,float lValue)
        {
            float p = Perimeter(fValue, sValue, lValue)/2;
            return (float)Math.Sqrt(p * (p - fValue) * (p - sValue) * (p - lValue));
        }
         public float  Perimeter(float fValue, float sValue, float lValue)
        {
            return fValue + sValue + lValue;
        }
          public float Perimeter (float fValue, float lValue)
        {
            return (fValue + lValue) * 2;
        }
         public float Square (float fValue, float lValue)
        {
            return fValue*lValue;
        }
    }
}
