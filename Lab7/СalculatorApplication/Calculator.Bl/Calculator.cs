﻿using System;
using System.Collections.Generic;

namespace CalculatorBl
{
    public class Calculator 
    {
        private delegate double OperationDelegate(double first, double second);
        private Dictionary<char,OperationDelegate> _operations;
        public Calculator()
        {
            _operations =new Dictionary<char,OperationDelegate>
                {
                    {'+',(firstNumber,secondNumber)=>firstNumber+secondNumber},
                    {'-',(firstNumber,secondNumber)=>firstNumber-secondNumber},
                    {'*',(firstNumber,secondNumber)=>firstNumber*secondNumber},
                    {'/',Divide},
                };
        }
        private double Divide(double firstNumber, double secondNumber)
        {
            if (secondNumber == 0)
                throw new DivideByZeroException("Error!Divide by zero.");
            return firstNumber / secondNumber;
        }
        public double ChoiceOperation(double firstNumber,char operation, double secondNumber)
        {
            if (!_operations.ContainsKey(operation))
                throw new Exception();
            return _operations[operation](firstNumber,secondNumber);
        }   
    }
}
