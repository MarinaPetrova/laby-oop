﻿using System;
using System.Collections.Generic;

namespace MyCollectionBl
{
    interface IMyCollection<TValue> 
    {
        List<TValue> List { get; set; }
        TValue this[int i] { get;set; }
        void AddValue(TValue choose);
        void SortCollection(IComparer<TValue> comparison, Func<TValue, bool> validateCount);
        string BinarySerialize();
        string BinaryDeserialize();
        string XmlSerialize();
        string XmlDeserialize();
    }
}
