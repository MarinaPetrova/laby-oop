﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MyCalculator
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void txtInput_MouseMove(object sender, MouseEventArgs e)
        {

        }

        private void btNumber_Click(object sender, RoutedEventArgs e)
        {
            var btNumber = sender as Button;
            if (btNumber != null)
            {
                var inputDate = txtInput.Text;
                if (inputDate.Equals("0"))
                {
                    inputDate = $"{btNumber.Tag}";
                }
                else
                {
                    inputDate = $"{inputDate}{btNumber.Tag}";
                }
                txtInput.Text = inputDate;
            }
        }
    }
}
