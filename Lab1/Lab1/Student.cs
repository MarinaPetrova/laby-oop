﻿using System;

public class Student
{
    private string _firstName;
    private string _lastName;
    private string _groupName;
    private string _univName;
    private int _age;
    public string FistName { get { return _firstName; } set { _firstName = value; } }
    public string LastName { get { return _lastName; } set { _lastName = value; } }
    public string GroupName{ get { return _groupName; } set { _groupName = value; } }
    public string UnivName { get { return _univName; } set { _univName = value; } }
    public int _Age {
        get { return _age; }
        set {
            if (value < 0 || value > 50)
            {
                throw new Exception("0<age<50\n");
            }
            else 
                _age = value;
            }
    }
   
    public void  WritVal()
    {
        Console.WriteLine("University:");
        _univName = Convert.ToString(Console.ReadLine());
        Console.WriteLine("Name:");
        _firstName = Convert.ToString(Console.ReadLine());
        Console.WriteLine("Last name:");
        _lastName = Convert.ToString(Console.ReadLine());
        Console.WriteLine("Group name:");
        _groupName = Convert.ToString(Console.ReadLine());
        do
        {
            Console.WriteLine("Age:");
            _age = Convert.ToInt32(Console.ReadLine());
        } while (_age < 0 || _age > 50);
    }

    public override string ToString()
    {
        return string.Format("{0}\t{1}\t{2}\t({3}):\t{4}", _univName, _firstName, _lastName, _groupName,_age);
    }
}