﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Xml.Serialization;

namespace MyCollectionBl
{
    [Serializable]
    public class MyCollection<TValue> : IMyCollection<TValue>, IEnumerable<TValue>
    {
        private List<TValue> _list;
        public List<TValue> List
        {
            get { return _list; }
            set { _list = value; }
        }
        public TValue this[int i]
        {
            get { return _list[i]; }
            set { _list[i]=value; }
        }
        public MyCollection()
        {
            _list = new List<TValue>();
        }
        IEnumerator<TValue> IEnumerable<TValue>.GetEnumerator()
        {
            return _list.GetEnumerator();
        }
        public IEnumerator GetEnumerator()
        {
            return GetEnumerator();
        }
        public void Add(object value)
        {
           _list.Add((TValue)value);
        }
        public void AddValue(TValue choose)
        {
            _list.Add(choose);
        }
        public void SortCollection(IComparer<TValue> comparison, Func<TValue,bool> validateCount)
        {
            var query = _list.Where(validateCount);
            _list = query.ToList<TValue>();
            _list.Sort(0, _list.Count,comparison);
        }
        public override string ToString()
        {
            StringBuilder result = new StringBuilder("Collection:");
            foreach (var value in _list)
                result.Append($"{value} ");
            return result.ToString();
        }
        public string BinarySerialize()
        {
            var formatter = new BinaryFormatter();
            using (var fs=new FileStream("collection.dat",FileMode.OpenOrCreate))
            {
                formatter.Serialize(fs,this);
            }
           return "Binary.Serialized " + this.ToString();
        }
        public string BinaryDeserialize()
        {
            var formatter = new BinaryFormatter();
            using (var fs = new FileStream("collection.dat", FileMode.OpenOrCreate))
            {
                var list=formatter.Deserialize(fs);
            }
            var result=new StringBuilder("Binary.The collection is deserialized: ");
            foreach (TValue value in List)
                result.Append($"{value} ");
            return result.ToString();
        }
        public string XmlSerialize()
        {
            var formatter = new XmlSerializer(typeof(List<TValue>));
            using (var fs = new FileStream("myCollection.xml", FileMode.OpenOrCreate))
            {
                formatter.Serialize(fs, this._list);
            }
            return "Xml.Serialized " + this.ToString();
        }
        public string XmlDeserialize()
        {
            var formatter = new XmlSerializer(typeof(List<TValue>));
            using (var fs = new FileStream("myCollection.xml", FileMode.OpenOrCreate))
            {
                var list =formatter.Deserialize(fs);
            }
            var result = new StringBuilder("Xml.The collection is deserialized: ");
            foreach (TValue value in List)
                result.Append($"{value} ");
            return result.ToString();
        }
    }
}
