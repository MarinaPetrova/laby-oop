﻿using System;
using FigureBALL;
using Lab_3.App.Key;

namespace Lab_3.App.Managers
{
    class MainManager
    {
        private readonly Figure _firstFigure;
        public MainManager()
        {           
            float aNumber;            
            float bNumber;            
            float cNumber;
            WriteValue(out aNumber, out bNumber, out cNumber);
            _firstFigure = new Figure(aNumber, bNumber, cNumber);
        }
        private void WriteValue( out float aNumber,out float bNumber,out float cNumber)
        {
            Console.WriteLine("First side: ");
            aNumber = Single.Parse(Console.ReadLine());
            Console.WriteLine("Second side: ");
            bNumber = Single.Parse(Console.ReadLine());
            Console.WriteLine("Third side: ");
            cNumber = Single.Parse(Console.ReadLine());
        }
        private void Show(TypeOfSample function)
        {
            float a = _firstFigure.Avalue;
            float b = _firstFigure.Svalue;
            float c = _firstFigure.Lvalue;
            switch (function)
            {
                case TypeOfSample.First:
                    Console.WriteLine("1)Square : " + _firstFigure.Square(a, b, c));
                    break;
                case TypeOfSample.Second:
                    Console.WriteLine("1)Perimeter : " + _firstFigure.Perimeter(a, b, c));
                    break;
                case TypeOfSample.Third:
                    Console.WriteLine("2)Square : " + _firstFigure.Square(a, b));
                    break;
                case TypeOfSample.Fourth:
                    Console.WriteLine("2)Perimeter : " + _firstFigure.Perimeter(a, b));
                    break;
            }
        }
        public void Choice ()
        {
            Console.WriteLine("Write number (1 - 4) : ");
            int i = Int32.Parse(Console.ReadLine());
            if (i < 5 && i > 0)
            {
                TypeOfSample key = (TypeOfSample)i;
                Show(key);
            }
            else
                throw new Exception("(1 - 4)");
        }
    }
}
