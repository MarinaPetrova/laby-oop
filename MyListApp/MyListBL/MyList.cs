﻿using System;
using System.Collections.Generic;

namespace MyListBL
{
    public class MyList<TValue>
    {
        public event EventHandler<ChangValueEventArgs<TValue>> AddValue;
        public event EventHandler<ChangValueEventArgs<TValue>> RemoveValue;
        public event EventHandler<ChangValueEventArgs<TValue>> AfterAddValue;
        public event EventHandler<ChangValueEventArgs<TValue>> AfterRemoveValue;
        private List<TValue> _list = new List<TValue>();
        protected void CallAddHandler(ChangValueEventArgs<TValue> e)
        {
            var evnt = AddValue;
            if (evnt != null)
                evnt(this,e);
            _list.Add(e.Value);
            evnt = AfterAddValue;
            if (evnt != null)
                evnt(this,e);
        }
        protected void CallRemoveHandler(ChangValueEventArgs<TValue> e)
        {
            var evnt = RemoveValue;
            if (evnt != null)
                evnt(this, e);
            _list.Remove(e.Value);
            evnt = AfterRemoveValue;
            if (evnt != null)
                evnt(this, e);
        }
        public void Add(TValue value)
        {
            var e = new ChangValueEventArgs<TValue>(value);
            CallAddHandler(e);
        }
        public void Remove(TValue value)
        {
            var e = new ChangValueEventArgs<TValue>(value);
            CallRemoveHandler(e);
        }
        public void Sort()
        {
            _list.Sort();
        }
        public override string ToString()
        {
            string result="";
            foreach (var b in _list)
                result+=b.ToString()+" ";
            return result; 
        }
    }
}
