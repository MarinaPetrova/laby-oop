﻿using MyListBL;
using System;


namespace MyListApp
{
    class Manager<TValue>
    {
        public Manager(MyList<TValue> list)
        {
            list.AddValue += AddHandler;
            list.AfterAddValue += AfterAddHandler;
            list.RemoveValue += RemoveHandler;
            list.AfterRemoveValue += AfterRemoveHandler;
        }
        private void AddHandler(object sender,ChangValueEventArgs<TValue> e)
        {
            if(e.GetType()==typeof(int))
            {
                ChangValueEventArgs<int> validate;
                object buffer =(object)e;
                if((validate=buffer as ChangValueEventArgs<int>)!=null && validate.Value%2!=0)
                {
                    validate.Value++;
                }
            }
        }
        private void RemoveHandler(object sender, ChangValueEventArgs<TValue> e)
        {
            if (e.Value.GetType() == typeof(int))
            {
                ChangValueEventArgs<int> validate;
                object buffer = (object)e;
                if ((validate = buffer as ChangValueEventArgs<int>) != null && validate.Value % 2 == 0)
                {
                    validate.Value--;
                }
            }
        }
        private void AfterAddHandler(object sender, ChangValueEventArgs<TValue> e)
        {
            Show(sender);
            if(e.Value.GetType() == typeof(int))
             Console.WriteLine($"Added value:{e.Value}");
        }
        private void AfterRemoveHandler(object sender, ChangValueEventArgs<TValue> e)
        {
            Show(sender);
            if (e.Value.GetType() == typeof(int))
                Console.WriteLine($"Remote value:{e.Value}");
        }
        private void Show(object sender)
        {
            var outList = (MyList<TValue>)sender;
            Console.WriteLine(outList.ToString());
        }
    }
}
