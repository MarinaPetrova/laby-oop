﻿using MyListBL;
using System;

namespace MyListApp
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                var list = new MyList<int>();
                var example = new Manager<int>(list);
                list.Add(70);
                list.Add(69);
                list.Remove(69);
                list.Remove(71);
                list.Add(80);
                list.Add(30);
                list.Sort();
                Console.WriteLine(list.ToString());
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                Console.ReadKey();
            }
        }
    }
}
