﻿using Calcul.BAL;
using System;

namespace Lab_2.Manager
{
    class MainManager
    {
        private Calculator _example;
        public MainManager()
        {
            Console.WriteLine("First number :");
            double fnumber = Double.Parse(Console.ReadLine());
            Console.WriteLine("Operation :");
            char operation = Char.Parse(Console.ReadLine());
            Console.WriteLine("Last number :");
            double lnumber = Double.Parse(Console.ReadLine());
            _example = new Calculator(fnumber,operation,lnumber);
        }
        public void Output()
        {
            Console.WriteLine(_example.Solution());
        }
    }
}
