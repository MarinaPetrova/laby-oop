﻿using System;
using Lab_3.App.Managers;

namespace Lab_3
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                WriteNumber();
            }
            catch (FormatException)
            {
                Console.WriteLine("\aError!");
            }
            catch (Exception ex)
            {
                Console.WriteLine("\aError !"+ ex.Message);
            }
            finally
            {
                Console.ReadKey();
            }
        }
        static void  WriteNumber()
        {
            MainManager myFigure = new MainManager();
            myFigure.Choice();
        }
    }
}
