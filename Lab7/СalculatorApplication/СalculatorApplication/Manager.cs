﻿using System;
using CalculatorBl;

namespace Сalculator.Application
{
    class Manager
    {
        private Calculator _example;
        public Manager()
        {        
           _example = new Calculator();
        }
        public void Result()
        {
            Console.Write("First number:");
            var first = Double.Parse(Console.ReadLine());
            Console.Write("Operation:");
            var operation = Char.Parse(Console.ReadLine());
            Console.Write("Second number:");
            var second = Double.Parse(Console.ReadLine());
            Console.WriteLine($"Result:{_example.ChoiceOperation(first,operation,second)}");
        }
    }
}
